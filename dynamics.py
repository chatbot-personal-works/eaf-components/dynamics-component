import requests
from logger.logger import logger
from logger.EAFLogDecorators import EAFClassDecorator, EAFMethodLogger
from pprint import pprint
import json

@EAFClassDecorator
class DynamicsIntegration:
    def __init__(self, config):
        self.dyn_url = config['resource']
        self.dynamics_token_url = config['token_url']
        self.dynamics_token_body = config.copy()
        del self.dynamics_token_body['token_url']
        self.token = self.get_oauth2_token(url=self.dynamics_token_url, body=self.dynamics_token_body)

    def check_status(self, code):
        if code == 204 or code == 200:         # Success
            return 0
        elif code == 401:       # Token Expired
            self.token = self.get_oauth2_token(
                url=self.dynamics_token_url, body=self.dynamics_token_body)     #Regenerate Token
            return 1
        else:                   # Failure
            return 2

    def get_oauth2_token(self, **kwargs):
        url = kwargs['url']
        body = kwargs['body']
        response = requests.post(url, data=body)
        out = response.json()
        token = out['access_token']
        return token

    def post_data(self, url, header, body):
        while True:
            res = requests.post(url=url, headers=header, json=body)
            return res
    
    def patch_data(self, url, header, body):
        while True:
            res = requests.patch(url=url, headers=header, json=body)
            pprint(res.text)
            return res


    def create_push_url(self, entity):
        if entity == "lead":
            url = "{}/api/data/v9.0/leads".format(self.dyn_url)
            header = {
                "Authorization": "Bearer {0}".format(self.token),
                "Accept": "application/json",
                "OData-MaxVersion": "4.0",
                "OData-Version": "4.0"
            }
            return url, header
        elif entity == "account":
            url = "{}/api/data/v9.1/accounts".format(self.dyn_url)
            header = {
                "Authorization": "Bearer {0}".format(self.token),
                "Accept": "application/json",
                "OData-MaxVersion": "4.0",
                "OData-Version": "4.0"
            }
            return url, header
        elif entity == "case":
            url = "{}/api/data/v9.1/incidents".format(self.dyn_url)
            header = {
                "Authorization": "Bearer {0}".format(self.token),
                "Accept": "application/json",
                "OData-MaxVersion": "4.0",
                "OData-Version": "4.0"
            }
            return url, header
        elif entity == "contacts":
            url = "{}/api/data/v9.1/contacts".format(self.dyn_url)
            header = {
                "Authorization": "Bearer {0}".format(self.token),
                "Accept": "application/json",
                "OData-MaxVersion": "4.0",
                "OData-Version": "4.0"
            }
            return url, header
        


    def create_pull_url(self, entity):
        if entity == "account":
            url = "{}/api/data/v9.1/accounts".format(self.dyn_url)
            header = {
                "Authorization": "Bearer {0}".format(self.token),
                "Accept": "application/json",
                "OData-MaxVersion": "4.0",
                "OData-Version": "4.0"
            }
            return url, header
        elif entity == "contacts":
            url = "{}/api/data/v9.1/contacts".format(self.dyn_url)
            header = {
                "Authorization": "Bearer {0}".format(self.token),
                "Accept": "application/json",
                "OData-MaxVersion": "4.0",
                "OData-Version": "4.0"
            }
            return url, header
        elif entity == "case":
            url = "{}/api/data/v9.1/incidents".format(self.dyn_url)
            header = {
                "Authorization": "Bearer {0}".format(self.token),
                "Accept": "application/json",
                "OData-MaxVersion": "4.0",
                "OData-Version": "4.0"
            }
            return url, header
        elif entity == "lead":
            url = "{}/api/data/v9.1/leads".format(self.dyn_url)
            header = {
                "Authorization": "Bearer {0}".format(self.token),
                "Accept": "application/json",
                "OData-MaxVersion": "4.0",
                "OData-Version": "4.0"
            }
            return url, header
    @EAFMethodLogger
    def create_lead(self, contact):
        dynamics_lead_url, dynamics_lead_header = self.create_push_url("lead")
        response = self.post_data(url=dynamics_lead_url, header=dynamics_lead_header, body=contact)
        if self.check_status(response.status_code) == 0:
            return {"Status": "Success", "StatusReason": "Successfully created a lead in Dynamics"}
        elif self.check_status(response.status_code) == 1:
            dynamics_lead_url, dynamics_lead_header = self.create_push_url(
                "lead")
            response = self.post_data(
                url=dynamics_lead_url, header=dynamics_lead_header, body=contact)
            if not self.check_status(response.status_code):
                return {"Status": "Success", "StatusReason": "Successfully created a lead in Dynamics"}
            else:
                return {"Status": "Failure", "StatusReason": "Failed to create a lead in Dynamics"}
        else:
            return {"Status": "Failure", "StatusReason": "Failed to create a lead in Dynamics"}


    @EAFMethodLogger
    def create_case(self,casedata):
        dynamics_case_url, dynamics_case_header = self.create_push_url("case")
        response = self.post_data(url=dynamics_case_url, header=dynamics_case_header, body=casedata)
        print(response.text)
        if self.check_status(response.status_code) == 0:
            return {"Status": "Success", "StatusReason": "Successfully created a ticket in Dynamics"}
        elif self.check_status(response.status_code) == 1:
            dynamics_case_url, dynamics_case_header = self.create_push_url(
                "case")
            response = self.post_data(
                url=dynamics_case_url, header=dynamics_case_header, body=casedata)
            if not self.check_status(response.status_code):
                return {"Status": "Success", "StatusReason": "Successfully created a ticket in Dynamics"}
            else:
                return {"Status": "Failure", "StatusReason": "Failed to create a ticket in Dynamics"}
        else:
            return {"Status": "Failure", "StatusReason": "Failed to create a ticket in Dynamics"}

    @EAFMethodLogger
    def create_contact(self,contactdata):
        dynamics_contact_url, dynamics_contact_header = self.create_push_url("contacts")
        response = self.post_data(url=dynamics_contact_url, header=dynamics_contact_header, body=contactdata)
        print("create_contact*************************************")
        id = response.headers.get('OData-EntityId')
        print(id)
        id = id.split("(")
        id = id[1].split(")")
        
        
        if self.check_status(response.status_code) == 0:
            return {"Status": "Success", "Data": id[0]}
        elif self.check_status(response.status_code) == 1:
            dynamics_contact_url, dynamics_contact_header = self.create_push_url(
                "contacts")
            response = self.post_data(
                url=dynamics_contact_url, header=dynamics_contact_header, body=contactdata)
            if not self.check_status(response.status_code):
                return {"Status": "Success", "Data": id[0]}
            else:
                return {"Status": "Failure", "StatusReason": "Failed to create a contact in Dynamics"}
        else:
            return {"Status": "Failure", "StatusReason": "Failed to create a contact in Dynamics"}

    
    @EAFMethodLogger
    def create_account(self, account):
        dynamics_account_url, dynamics_account_header = self.create_push_url(
            "account")
        response = self.post_data(
            url=dynamics_account_url, header=dynamics_account_header, body=account)
        if self.check_status(response.status_code) == 0:
            return {"Status": "Success", "StatusReason": "Successfully created an account in Dynamics"}
        elif self.check_status(response.status_code) == 1:
            dynamics_account_url, dynamics_account_header = self.create_push_url(
                "account")
            response = self.post_data(
                url=dynamics_account_url, header=dynamics_account_header, body=account)
            if not self.check_status(response.status_code):
                return {"Status": "Success", "StatusReason": "Successfully created an account in Dynamics"}
            else:
                return {"Status": "Failure", "StatusReason": "Failed to create an account in Dynamics"}
        else:
            return {"Status": "Failure", "StatusReason": "Failed to create an account in Dynamics"}

    def get_account(self):
        dynamics_account_url, dynamics_account_header = self.create_pull_url("account") 
        response = requests.get(url=dynamics_account_url,
                                headers=dynamics_account_header)
        if self.check_status(response.status_code) == 0:
            return {"Status": "Success", "Data": response.json()}
        elif self.check_status(response.status_code) == 1:
            dynamics_account_url, dynamics_account_header = self.create_pull_url(
                "account")
            response = requests.get(url=dynamics_account_url,
                                    headers=dynamics_account_header)
            if not self.check_status(response.status_code):
                return {"Status": "Success", "Data": response.json()}
            else:
                return {"Status": "Failure", "StatusReason": "Failed to pull account from Dynamics"}
        else:
            return {"Status": "Failure", "StatusReason": "Failed to pull account from Dynamics"}

    @EAFMethodLogger
    def get_lead(self,email):
        logger.info('Get lead Function started')
        xmlstrng = f'<fetch distinct="false" mapping="logical" output-format="xml-platform" version="1.0"> \
        <entity name="lead"> \
        <attribute name="fullname"/> \
        <attribute name="createdon"/>\
        <attribute name="statuscode"/>\
        <attribute name="subject"/>\
        <attribute name="ownerid"/>\
        <attribute name="leadid"/>\
        <attribute name="new_courselist"/>\
        <order descending="true" attribute="createdon"/>\
        <filter type="and">\
        <condition attribute="emailaddress1" value=\"{email}\" operator="eq"/>\
        </filter>\
        </entity>\
        </fetch>'
        dynamics_lead_url, dynamics_lead_header = self.create_pull_url("lead")
        nw_url  = dynamics_lead_url+"?fetchXml="+ xmlstrng
        response = requests.get(url=nw_url,headers=dynamics_lead_header)
        print(response.status_code)
        if self.check_status(response.status_code) == 0:
            return {"Status": "Success", "Data": response.json()}
        elif self.check_status(response.status_code) == 1:
            dynamics_lead_url, dynamics_lead_header = self.create_pull_url(
                "lead")
            response = requests.get(url=dynamics_lead_url,
                                    headers=dynamics_lead_header)
            if not self.check_status(response.status_code):
                return {"Status": "Success", "Data": response.json()}
            else:
                return {"Status": "Failure", "StatusReason": "Failed to pull lead from Dynamics"}
        else:
            return {"Status": "Failure", "StatusReason": "Failed to pull lead from Dynamics"}

    @EAFMethodLogger
    def get_contact(self, email):
        logger.info('Get contact Function started')
        xmlstrng = f'<fetch distinct="false" mapping="logical" output-format="xml-platform" version="1.0"> \
        <entity name="contact"> \
        <attribute name="fullname"/> \
        <attribute name="telephone1"/>\
        <attribute name="contactid"/>\
        <order descending="false" attribute="fullname"/>\
        <filter type="and">\
        <condition attribute="emailaddress1" value=\"{email}\" operator="eq"/>\
        </filter>\
        </entity>\
        </fetch>'
        dynamics_contact_url, dynamics_contact_header = self.create_pull_url("contacts")
        nw_url  = dynamics_contact_url+"?fetchXml="+ xmlstrng
        response = requests.get(url=nw_url,headers=dynamics_contact_header)
        
        if self.check_status(response.status_code) == 0:
            return {"Status": "Success", "Data": response.json()}
        elif self.check_status(response.status_code) == 1:
            dynamics_contact_url, dynamics_contact_header = self.create_pull_url(
                "contacts")
            response = requests.get(url=dynamics_contact_url,
                                    headers=dynamics_contact_header)
            if not self.check_status(response.status_code):
                return {"Status": "Success", "Data": response.json()}
            else:
                return {"Status": "Failure", "StatusReason": "Failed to pull lead from Dynamics"}
        else:
            return {"Status": "Failure", "StatusReason": "Failed to pull lead from Dynamics"}

    def update_lead(self,leadid,updatefield,newvalue):
        logger.info('Update lead Function started')
        dynamics_lead_url, dynamics_lead_header = self.create_push_url("lead")
        nw_url = dynamics_lead_url+"("+leadid+")"
        nw_url = nw_url+"/"+updatefield
        print(nw_url)
        response = self.patch_data(
            url=nw_url, header=dynamics_lead_header, body={"value":newvalue})
        print(response.status_code)
        if self.check_status(response.status_code) == 0:
            return {"Status": "Success", "StatusReason": "Successfully updated lead in Dynamics"}
        elif self.check_status(response.status_code) == 1:
            dynamics_lead_url, dynamics_lead_header = self.create_push_url(
                "lead")
            response = self.patch_data(
                url=dynamics_lead_url, header=dynamics_lead_header, body={})
            if not self.check_status(response.status_code):
                return {"Status": "Success", "StatusReason": "Successfully updated lead in Dynamics"}
            else:
                return {"Status": "Failure", "StatusReason": "Failed to update lead in Dynamics"}
        else:
            return {"Status": "Failure", "StatusReason": "Failed to update lead in Dynamics"}